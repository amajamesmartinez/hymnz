/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 */

import React from 'react';

import AppContainer from './app/navigations/AppNavigation';

function App(): React.JSX.Element {
  return <AppContainer />;
}

export default App;
