import React from 'react';

import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
// import {createDrawerNavigator} from '@react-navigation/drawer';
import {HomeScreen} from '../screens/home';
import {HymnsScreen} from '../screens/hymns';

const Stack = createStackNavigator();
const MainNavigator = () => {
  return (
    <Stack.Navigator>
      <Stack.Screen
        name="Home"
        component={HomeScreen}
        options={{headerShown: false}}
      />
      <Stack.Screen
        name="Hymns"
        component={HymnsScreen}
        // options={{headerShown: false}}
      />
    </Stack.Navigator>
  );
};

// const Drawer = createDrawerNavigator();
// const DrawerStack = () => {
//   return (
//     <Drawer.Navigator>
//       <Drawer.Screen name="Main" component={MainNavigator} />
//     </Drawer.Navigator>
//   );
// };

const AppContainer = () => {
  return (
    <NavigationContainer>
      <MainNavigator />
    </NavigationContainer>
  );
};

export default AppContainer;
