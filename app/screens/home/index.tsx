import React from 'react';
import {Text, StyleSheet, View, Pressable} from 'react-native';

export const HomeScreen = ({navigation}: any) => {
  return (
    <View style={Styles.container}>
      {/* HEADER */}
      <Text style={Styles.header}>Presbyterian Hymns</Text>
      {/* BUTTONS */}
      <View style={Styles.btnGroup}>
        <Pressable
          style={Styles.btn}
          onPress={() => navigation.navigate('Hymns')}>
          <Text style={Styles.text}>Hymns</Text>
        </Pressable>
        <Pressable
          style={Styles.btn}
          onPress={() => navigation.navigate('Hymns')}>
          <Text style={Styles.text}>Responsive Reading</Text>
        </Pressable>
        <Pressable
          style={Styles.btn}
          onPress={() => navigation.navigate('Hymns')}>
          <Text style={Styles.text}>Catechism</Text>
        </Pressable>
        <Pressable
          style={Styles.btn}
          onPress={() => navigation.navigate('Hymns')}>
          <Text style={Styles.text}>Credits</Text>
        </Pressable>
      </View>
    </View>
  );
};

const Styles = StyleSheet.create({
  header: {
    textAlign: 'center',
    alignItems: 'center',
    letterSpacing: 3,
    fontSize: 48,
    marginBottom: 48,
  },
  container: {
    paddingTop: 100,
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    width: '100%',
  },
  btnGroup: {
    display: 'flex',
    width: '100%',
    padding: 0,
    // backgroundColor: 'red',
    gap: 8,
    alignItems: 'flex-start',
  },
  btn: {
    alignItems: 'flex-start',
    justifyContent: 'center',
    paddingVertical: 12,
    paddingHorizontal: 32,
    width: 340,
    borderRadius: 4,
    elevation: 3,
    backgroundColor: 'black',
  },
  text: {
    fontSize: 24,
    lineHeight: 32,
    fontWeight: 'bold',
    letterSpacing: 1,
    color: 'white',
  },
});

export default HomeScreen;
