import React from 'react';
import {Text, StyleSheet, View} from 'react-native';

export const HymnsScreen = () => {
  return (
    <View style={Styles.container}>
      <Text style={styles.header}>Hymns Screen</Text>
    </View>
  );
};

const styles = StyleSheet.create({
  header: {
    textAlign: 'center',
    alignItems: 'center',
    backgroundColor: 'red',
    fontSize: 48,
  },
});

const Styles = StyleSheet.create({
  header: {
    textAlign: 'center',
    alignItems: 'center',
    backgroundColor: 'red',
    fontSize: 48,
  },
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
});

export default HymnsScreen;
